using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Prime.Services
{
    public class StringUtils
    {

        public Dictionary<string,int> charCount(string v)
        {
            var result = new Dictionary<string, int>();
            foreach(char a in RemoveDiacritics(v.ToLower())){
                var aString = a.ToString(); 
                if(result.ContainsKey(aString)){
                    result[aString] +=1; 
                }else{
                    result.Add(aString,1);
                }
            }
            return result;
        }


        static string RemoveDiacritics(string text) 
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}