using System;

namespace Stm.Services
{
    public class Tarjeta
    {
        public int Id { get; private set; }
        public int Saldo { get; private set; }

        public bool Jubilado { get; set; }
        public Tarjeta(int id){
            Id = id;
            Saldo = 0;
            Jubilado = false;
        }

        public void Cargar(int saldo){
            Saldo += saldo;
        }

        public void Consumir(int saldo){
            Saldo -= saldo;
        }
    }
}