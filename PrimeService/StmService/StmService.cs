using System;
using System.Collections.Generic;

namespace Stm.Services
{
    public class StmService
    {
        private const int DESCUENTO_JUBILADO = 14;
        Dictionary<int,Tarjeta> tarjetas;
        int IdsTarjetas = 0;
        public StmService(){
            tarjetas = new Dictionary<int,Tarjeta>();
        }

        public void Pagar(int idTarjeta, int costo, int idTransporte, DateTime fecha){
            Tarjeta t = tarjetas[idTarjeta];
            var costoFinal = costo;
            if(t.Jubilado){
                costoFinal-= DESCUENTO_JUBILADO;
            }
            t.Consumir(costoFinal);            
        }

        public void CargarTarjeta(int idTarjeta, int saldo)
        {
            Tarjeta t = tarjetas[idTarjeta];
            t.Cargar(saldo);
        }

        public int GetSaldoTarjeta(int idTarjeta)
        {
            return tarjetas[idTarjeta].Saldo;
        }

        public int NuevaTarjeta()
        {
            IdsTarjetas+=1;
            Tarjeta t = new Tarjeta(IdsTarjetas);
            tarjetas.Add(IdsTarjetas,t);
            return IdsTarjetas;
        }

        public void HacerTarjetaJubilado(int idTarjeta)
        {
            tarjetas[idTarjeta].Jubilado=true;
        }
    }
}