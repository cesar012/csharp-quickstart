using NUnit.Framework;
using Prime.Services;
using System;
using System.Collections.Generic;

namespace Prime.UnitTests.Services
{
    [TestFixture]
    public class StringUtilsTest
    {
    
        [Test]
        public void CharCountEmpty()
        {
            StringUtils strUtils = new StringUtils();
            var result = strUtils.charCount("");

            Assert.IsTrue(result.Count==0,"Should be empty");
        }

        [Test]
        public void CharCountOneChar()
        {
            StringUtils strUtils = new StringUtils();
            var result = strUtils.charCount("a");

            Assert.IsTrue(result.Count==1 && result["a"] ==1,"Should be [(a:1)]");
        }

        [Test]
        public void CharCountMama()
        {
            StringUtils strUtils = new StringUtils();
            var result = strUtils.charCount("mama");

            Assert.IsTrue(result.Count==2 && result["a"] ==2 && result["m"] ==2,"Should be [(m:2),(a:2)]");
        }

        [Test]
        public void CharCountMamaMayus()
        {
            StringUtils strUtils = new StringUtils();
            var result = strUtils.charCount("Mama");

            Assert.IsTrue(result.Count==2 && result["a"] ==2 && result["m"] ==2, "Should be [(m:2),(a:2)]");
        }

        [Test]
        public void CharCountMamaMayusDiac()
        {
            StringUtils strUtils = new StringUtils();
            var result = strUtils.charCount("Mamá");

            Assert.IsTrue(result.Count==2 && result["a"] ==2 && result["m"] ==2, "Should be [(m:2),(a:2)]");
        }
    }
}