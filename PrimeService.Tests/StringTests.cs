using NUnit.Framework;
using Prime.Services;
using System;

namespace Prime.UnitTests.Services
{
    [TestFixture]
    public class StringTests
    {

        #region Sample_TestCode
        [TestCase("")]
        [TestCase("Hello World")]
        [TestCase("Prueba")]
        public void NotNull(String value)
        {
            var result = value!=null;

            Assert.IsTrue(result, $"{value} should not be null");
        }

        [TestCase("","","")]
        [TestCase("Hello ","World", "Hello World")]
        [TestCase("Pru","eba", "Prueba")]
        public void Concat(String value1,String value2, String expectedResult)
        {
            var concat = value1+value2;

            Assert.AreEqual(concat,expectedResult,"Not equals");
        }


        [TestCase("",0)]
        [TestCase("Hello",5)]
        public void Length(String value1,int largo)
        {
            var len = value1.Length;

            Assert.AreEqual(len, largo,"Not equal length");
        }


        [TestCase("",0)]
        public void CharCount(String value1,int largo)
        {
            var len = value1.Length;

            Assert.AreEqual(len, largo,"Not equal length");
        }

        #endregion
    }
}