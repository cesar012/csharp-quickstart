using NUnit.Framework;
using Prime.Services;
using Stm.Services;
using System;
using System.Collections.Generic;

namespace Stm.UnitTests.Services
{
    [TestFixture]
    public class StmServiceTest
    {
    
        [Test]
        public void Pagar29()
        { 
            StmService service = new StmService();
            int idTarjeta = service.NuevaTarjeta();
            service.CargarTarjeta(idTarjeta,29);
            service.Pagar(idTarjeta, 29,-1,DateTime.Now);
            Assert.IsTrue(service.GetSaldoTarjeta(idTarjeta)==0,"Should be 0");
        }

        [Test]
        public void PagarJubilado()
        { 
            StmService service = new StmService();
            int idTarjeta = service.NuevaTarjeta();
            service.CargarTarjeta(idTarjeta,30);
            service.HacerTarjetaJubilado(idTarjeta);
            service.Pagar(idTarjeta, 29,-1,DateTime.Now);
            Assert.IsTrue(service.GetSaldoTarjeta(idTarjeta)==15,"Should be 15");
        }

    }
}